import BMIResult from '../components/BMIResult.js'
import {useState} from "react";

function BMICalPage() {

    const [ name, setName ] = useState("");
    const [ bmiResult, setBmiResult ] = useState("");
    const [ TranslateResult, setTranslateResult ] = useState("");

    const [height, setHeight] = useState("");
    const [weight, setWeight] = useState("");

    function calculateBMI(){
        let h = parseFloat(height);
        let w = parseFloat(weight);
        let bmi = w/(h*h);
        setBmiResult(bmi);
        if(bmi>25){
            setTranslateResult("อ้วนแล้วน้าา");
        }else if(bmi<20){
            setTranslateResult("ขยัน แด๊ก หน่อย");
        }else{
            setTranslateResult("เนี่ยต้องหุ่นแบบนี้");
        }
    }

    return(
    <div align = "left">
        <div align = "center">
            <br/>
            <h1>เว๊บคำนวณ BMI ที่ ตะมุตะมิ</h1>
            <br/>

            <h2>ชื่อ : <input type = "text" value={name} onChange={(e) => { setName(e.target.value);}}/>
            </h2>

            <br/>
            <h3>ส่วนสูง (m) : <input type = "text" value={height} onChange={(e) =>{setHeight(e.target.value);}}/>
            </h3>

            <h3>น้ำหนัก (kg) : <input type = "text" value={weight} onChange={(e) => {setWeight(e.target.value);}}/></h3>
            <br />
            <button onClick={(e) => {calculateBMI()}}>คำนวณ</button>
            <br/>
            <br/>
            <br/>

            { bmiResult != 0 && 
                <div>
                    <h3>ผลชันสูตรมาแล้วจ้า</h3>
                    <BMIResult
                        name ={ name }
                        BMI = { bmiResult }
                        result = {TranslateResult}
                    />
                </div>
            }
                    
        </div>
    </div>
    );
}

export default BMICalPage;