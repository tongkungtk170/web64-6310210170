import {Link} from "react-router-dom"

import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';

export default function ButtonAppBar() {
  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static">
        <Toolbar>
       
          <IconButton
            size="large"
            edge="start"
            color="inherit"
            aria-label="menu"
            sx={{ mr: 2 }} >
            <MenuIcon />
            <Link to="/">คำนวณBMI</Link>
                &nbsp; &nbsp; &nbsp;
                <Link to="about">ผู้จัดทำ</Link>
                &nbsp; &nbsp; &nbsp;
                <Link to="/contact">ติดต่อ</Link>
                &nbsp; &nbsp; &nbsp;
                <Link to="/LuckyNumber">สุ่มเลข</Link>
          </IconButton>
          <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
          </Typography>
          <Button color="inherit">Login</Button>
        </Toolbar>
      </AppBar>
    </Box>
  );
}


/*function Header() {

    return (
        <div align = "left">
            <p>เว็บคำนวณ BMI แบบกาวๆ : &nbsp;
                <Link to="/">เครื่องคิดเลข</Link>
                &nbsp; &nbsp; &nbsp;
                <Link to="about">ผู้จัดทำ</Link>
                &nbsp; &nbsp; &nbsp;
                <Link to="/contact">ติดต่อ</Link>
                &nbsp; &nbsp; &nbsp;
                <Link to="/LuckyNumber">สุ่มเลข</Link>
            </p>
        </div>
    );
}

export default Header;*/