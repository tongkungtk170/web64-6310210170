import LuckyNumber from '../components/LuckyNumber'
import {useState, useEffect} from "react";

function LuckyNumberPage (){

    const [sentence, setSentence] =  useState("");
    const [num, setNum] = useState();

    function RandomNumber(){
        let n = parseInt(num);
        setNum(n);
        if(n == 59){
            setSentence("ถูกแล้วนะ");
        }else if((n>=0) && (n<=99)){
            setSentence("ผิด");
        }else{
            setSentence("ทายตัวเลขตามที่ระบุไว้");
        }
    }

    return (
        <div align = "center">
            <h1>สุ่มเลขกันเถอะ</h1><br/>
            <h2>อยากพิมเลขไรก็พิมมา 0-99 : <input type = "text" value={num} onChange={(e) => { setNum(e.target.value);}}/></h2>
            <button onClick={(e) => {RandomNumber()}}>ทาย</button>

            { num != 0 &&
                <div>
                    <br/>
                    <LuckyNumber
                            random = {sentence}
                    />
                </div>
            }        
        </div>
    );

}


export default LuckyNumberPage;